from Tkinter import *
import tkFileDialog
from ttk import Frame, Style
import tkFont
from alg import edit_distance as ed
from alg import hamming_distance as hd
from alg import needleman_wunsch as nw
from alg import smith_waterman as sw
import table_gui
from matrices.matrices import *

list_matrices = ['benner22', 'benner6', 'benner74', 'blosum100', 'blosum30', 'blosum35',
                 'blosum40', 'blosum45', 'blosum50', 'blosum55', 'blosum60', 'blosum62',
                 'blosum65', 'blosum70', 'blosum75', 'blosum80', 'blosum85', 'blosum90',
                 'blosum95', 'feng', 'fitch', 'genetic', 'gonnet', 'grant', 'ident', 'johnson',
                 'levin', 'mclach', 'miyata', 'nwsgappep', 'pam120', 'pam180', 'pam250',
                 'pam30', 'pam300', 'pam60', 'pam90', 'rao', 'risler', 'structure']


class MainWindow(Frame):
    """This is the main window for program.
    Contains
    =========
    -- 2 textbox widgets which serves for inserting sequences for alignment
    -- checkboxes for choosing method of alignment
    -- checkbox for choosing if should be done backtracking
    -- menu for choosing substitution matrix
    --
    """
    def __init__(self, parent):
        """Initial settings for the window frame

        Attributes:
            parent (Frame object): Its the object of the gui window."""
        Frame.__init__(self, parent)
        self.parent = parent
        parent.minsize(width=1250, height=400)
        parent.maxsize(self.parent.winfo_screenwidth(), self.parent.winfo_screenheight())

        # initialization of variables
        #
        # mode keeps information which checkbutton is checked
        self.mode = StringVar()
        # keeps information if user checked backtracking option
        self.backtracking = BooleanVar()

        # name of the program
        self.parent.title("Visualization tool for sequence alignment")

        # settings for window style
        self.style = Style()
        self.style.theme_use('winnative')

        # list of fonts for textboxes + button
        self.bfont = tkFont.Font(family='Consolas', size=20)
        self.mfont = tkFont.Font(family='Consolas', size=12)
        self.popfont = tkFont.Font(family='Consolas', size=12, weight="bold")
        self.pack(fill=BOTH, expand=1)

        #
        # Frames
        #
        # F1
        #
        # This frame holds main part of the window
        # checkboxes, menu, button
        #
        frame = Frame(self, borderwidth=1, width=100, height=100)
        frame.grid_propagate(False)
        frame.pack(side="left", fill="both", expand=True)
        frame.grid_rowconfigure(1, weight=1)
        frame.grid_rowconfigure(3, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        frame.grid_columnconfigure(1, weight=1)
        frame.grid_columnconfigure(2, weight=1)

        # label Sequence 1
        lbl1 = Label(frame, text="Sequence 1")
        lbl1.grid(row=0, column=0,sticky=W, pady=4, padx=15)

        # textbox for sequence 1
        self.area1 = Text(frame, font=self.mfont)
        self.area1.grid(row=1, column=0, columnspan=4,
                        padx=15, sticky=E+W+S+N)

        # label Sequence 2
        lbl2 = Label(frame, text="Sequence 2")
        lbl2.grid(row=2, column=0, sticky=W, pady=4, padx=15)

        # textbox for Sequence 2
        self.area2 = Text(frame, font=self.mfont)
        self.area2.grid(row=3, column=0, columnspan=4,
                        padx=15, sticky=E+W+S+N)

        # checkbuttons for choosing method of aligment
        #
        cb_hd = Checkbutton(frame, text="Hamming distance",
                            variable=self.mode, onvalue="hamming_distance", command=self.on_check)
        cb_hd.select()
        cb_hd.grid(row=4, column=0, sticky=W, pady=10, padx=10)

        cb_ed = Checkbutton(frame, text="Edit distance",
                            variable=self.mode, onvalue="edit_distance", command=self.on_check)
        cb_ed.grid(row=4, column=1, sticky=W, pady=10, padx=10)

        cb_nw = Checkbutton(frame, text="Needleman_wunsch",
                            variable=self.mode, onvalue="needleman_wunsch", command=self.on_check)
        cb_nw.grid(row=4, column=2, sticky=W, pady=4, padx=10)

        cb_sw = Checkbutton(frame, text="Smith_waterman",
                            variable=self.mode, onvalue="smith_waterman", command=self.on_check)
        cb_sw.grid(row=4, column=3, sticky=W, pady=4, padx=10)

        cb_sw = Checkbutton(frame, text="Show backtracking",
                            variable=self.backtracking, onvalue=True, offvalue=False)
        cb_sw.grid(row=6, column=2, sticky=W, pady=4, padx=15)

        #
        # option fields for NW and SW alg. which shows after checking their checkbuttons
        #

        # label for matrix
        self.lbl3 = Label(frame, text="Matrix")
        self.lbl3.grid(row=6, column=0, sticky=E, pady=4, padx=10)
        self.lbl3.config(state="disable")

        # menu with list of available matrices
        # default value Blosum62
        self.menu_matrices = StringVar()
        self.menu_matrices.set("blosum62")
        self.opt_menu = apply(OptionMenu, (frame, self.menu_matrices) + tuple(list_matrices))
        self.opt_menu.config(state="disabled")
        self.opt_menu.grid(row=6, column=1, sticky=W, pady=4, padx=10)

        # label for for gap open penalty
        self.lbl4 = Label(frame, text="Gap open")
        self.lbl4.grid(row=7, column=0, sticky=W, pady=4, padx=10)
        self.lbl4.config(state="disable")

        # label for gap extend penalty
        self.lbl5 = Label(frame, text="Gap extend")
        self.lbl5.grid(row=7, column=1, sticky=W, pady=4, padx=10)
        self.lbl5.config(state="disable")

        # entry window for gap open penalty
        # default value is set to 10
        self.entry_gpopen = Entry(frame)
        self.entry_gpopen.insert(0, "10")
        self.entry_gpopen.config(state="disable")
        self.entry_gpopen.grid(row=8, column=0, sticky=W, pady=4, padx=15)

        # entry window for gap extend penalty
        # default value is set to 1
        self.entry_gpextend = Entry(frame)
        self.entry_gpextend.insert(0, "1")
        self.entry_gpextend.config(state="disable")
        self.entry_gpextend.grid(row=8, column=1, sticky=W, pady=4, padx=10)

        # run button
        run_button = Button(frame, text="Run", font=self.bfont, command=self.run)
        run_button.grid(row=7, column=3, rowspan=2)

        # fake row of the window made from label
        separator1 = Label(frame, text="")
        separator1.grid(row=10, column=0, columnspan=2, padx=10, sticky=E+W+S+N)

        #
        #
        # F2
        #
        # Frame2 contain only output text window

        frame2 = Frame(self, borderwidth=1, width=200, height=100)
        frame2.grid_propagate(False)
        frame2.pack(side="right", fill="both", expand=True)
        frame2.grid_rowconfigure(1, weight=1)
        frame2.grid_columnconfigure(0, weight=1)

        # label for output
        lbl3 = Label(frame2, text="Output")
        lbl3.grid(row=0, column=0, sticky=W, pady=4, padx=15)

        # scollbar for y axis
        scrollbar1 = Scrollbar(frame2)
        scrollbar1.grid(row=1, column=1, stick="nsew")

        # scrollbar for x axis
        scrollbar2 = Scrollbar(frame2, orient=HORIZONTAL)
        scrollbar2.grid(row=2, column=0, columnspan=2, stick="nsew", padx=15)

        # text box for output
        self.area3 = Text(frame2, font=self.mfont, wrap=NONE, xscrollcommand=scrollbar2.set,
                    yscrollcommand=scrollbar1.set)
        self.area3.grid(row=1, column=0, columnspan=2, padx=15, sticky=E+W+S+N)

        scrollbar2.config(command=self.area3.xview)
        scrollbar1.config(command=self.area3.yview)

        # fake row of the window made from label
        separator2 = Label(frame2, text="")
        separator2.grid(row=3, column=0, columnspan=2, padx=15, sticky=E+W+S+N)


        #
        # file dialog for sequence1 and sequence 2
        #
        menubar = Menu(self.parent)
        self.parent.config(menu=menubar)

        file_menu = Menu(menubar)
        file_menu.add_command(label="Load sequence 1", command=self.on_open)
        file_menu.add_command(label="Load sequence 2", command=self.on_open2)
        menubar.add_cascade(label="File", menu=file_menu)

    def run(self):
        """Method which calls sellected method with inserted parameters."""
        # checking what method was selected
        method = self.mode.get()
        # if backtracking was selected
        backtracking = bool(self.backtracking.get())
        # processing sequences see method @read_sequence
        seq1 = self.read_sequence(self.area1.get(1.0, END))
        seq2 = self.read_sequence(self.area2.get(1.0, END))


        # running selected method
        if method == "hamming_distance":
            # for Hamming distance are needed sequences of same length
            if len(seq1) != len(seq2):
                # popup window with the warning
                self.popup("Sequences are not the same length.")
            else:
                # run if everything is ok
                results = hd.hamming_distance(seq1, seq2)
                self.show_alignment(results)

        elif method == "edit_distance":

            result = ed.edit_distance(seq1, seq2, backtracking)
            if backtracking:
                self.show_results(result[1], result[0])
                self.show_alignment(result[0])
            else:
                self.show_results(result[1])

        else:
            # inicializing variables which are relevant only for NW and SW
            matrix = self.menu_matrices.get()
            # gaps have to be checked if they are numbers
            gap_open = self.check_if_digit(self.entry_gpopen.get())
            gap_extend = self.check_if_digit(self.entry_gpextend.get())
            # seqences are scanned, for case they have letter which is not in selected substitution matrix
            parsed_seq1 = self.check_if_letter_in_matrix(seq1, matrix)
            parsed_seq2 = self.check_if_letter_in_matrix(seq2, matrix)

            # if everything is ok/true run the selected method
            if gap_open is not None and gap_extend is not None and parsed_seq1 and parsed_seq2:

                if method == "needleman_wunsch":

                    result = nw.needleman_wunsch(seq1, seq2, gap_open, gap_extend, matrix, backtracking)
                    if backtracking:
                        self.show_results(result[3], result[5])
                        self.show_alignment(result[4])
                    else:
                        self.show_results(result)
                else:
                    result = sw.smith_waterman(seq1, seq2, gap_open, gap_extend, matrix, backtracking)

                    if backtracking:
                        self.show_results(result[3], result[5])
                        self.show_alignment(result[4])
                    else:
                        self.show_results(result)

    def show_results(self, result, backtracking_data=None):
        """Method which creates new window, where appears table with the values from matrices
        created in alignments algorithms.

        Attributes:
            result(dic): Dictionary with rows and columns of matrix - {row:{column=:value...}}
            backtracking_data (list): Contains coordinates of path in matrix. [(path1), (path2)..]
        """
        window = Toplevel(self)
        window.geometry("800x800")
        tab = table_gui.ResultsTable(window)
        tab.create_widgets(result, backtracking_data)
        tab.pack(fill="both", expand=True)

    def show_alignment(self, paths):
        """Method for displaying alignment of sequences for selected method.

        Attributes:
            paths(list): list of paths with pairs of sequences
        """
        self.area3.delete(1.0, END)
        if self.mode.get() == "edit_distance":
            self.area3.delete(1.0, END)
            self.area3.insert(2.0, "\nEdit distance\n")
            for path in paths:
                s1 = "\n"
                s2 = "\n"
                path.reverse()
                for pair in path:

                    s1 += pair[3]
                    s2 += pair[2]

                s1 += "\n"
                self.area3.insert(3.0, s1)
                self.area3.insert(3.0, s2)

        elif self.mode.get() == "hamming_distance":
            seq1 = self.read_sequence(self.area1.get(1.0, END))
            seq2 = self.read_sequence(self.area2.get(1.0, END))
            self.area3.delete(1.0, END)
            self.area3.insert(2.0, "\nHamming distance is %s.\n" % paths[0])
            self.area3.tag_configure("match", background="#FF0066")
            #self.area3.insert(3.0, END)
            for i in range(len(seq1)):
                print seq1[i]
                if i not in paths[1]:

                    self.area3.insert("3.end", seq1[i])

                else:
                    tag = "match"
                    self.area3.insert("3.end", seq1[i], tag)

            self.area3.insert(4.0, "\n")
            for i in range(len(seq1)):
                if i not in paths[1]:

                    self.area3.insert("4.end", seq2[i])
                else:
                    tag = "match"

                    self.area3.insert("4.end", seq2[i], tag)
        else:
            if self.mode.get() == "needleman_wunsch":
                self.area3.insert(1.0, "Alignment for Needleman Wunsch algorithm.\n")
                self.area3.insert(2.0, "Affine type of gap penalty.\n")
                self.area3.insert(3.0, "Penalty for opening gap = %s.\n" % self.entry_gpopen.get())
                self.area3.insert(4.0, "Penalty for extending gap = %s.\n" % self.entry_gpextend.get())
            else:
                self.area3.insert(1.0, "Alignment for Smith Waterman algorithm.\n")
                self.area3.insert(2.0, "Affine type of gap penalty.\n")
                self.area3.insert(3.0, "Penalty for opening gap = %s.\n" % self.entry_gpopen.get())
                self.area3.insert(4.0, "Penalty for extending gap = %s.\n" % self.entry_gpextend.get())
            # NW and SW
            s1 = "\n"
            s2 = "\n"
            paths.reverse()
            for pair in paths:

                s1 += pair[1]
                s2 += pair[0]

            s2 += "\n"
            self.area3.insert(5.0, s1)
            self.area3.insert(6.0, s2)


    def popup(self, warning):
        """Method for warning pop up windows.

        Attributes:
            warning (str): Message which is showed on popup window.
        """
        toplevel = Toplevel()
        w = self.parent.winfo_screenwidth()
        h = self.parent.winfo_screenheight()
        # getting numbers for centering popup window
        x = (w - 250)/2
        y = (h - 60)/2
        toplevel.geometry('%dx%d+%d+%d' % (250, 60, x, y))
        # label with the text from @warning
        label1 = Label(toplevel, text=warning, height=0, width=50, font=self.mfont)
        label1.pack()
        b = Button(toplevel, text="Close", width=20, command=toplevel.destroy)
        b.pack(side='bottom',padx=0,pady=0)

    def check_if_letter_in_matrix(self, sequence, matrix):
        """Method tests if in sequence isn't letter which isn't in substitution matrix

        If test fails, popup window appears and selected method wont run.
        Attributes:
            sequence (str): tested sequence
            matrix (dic): tested matrix

        Returns:
            bool: False in case if letter from @sequence isn't in @matrix
                        @sequence if all letters are in @ matrix
        """
        # list of all letters in matrix
        list_amk = []
        for keys in eval(matrix).keys():
            for key in keys:
                if key[0] not in list_amk:
                    list_amk.append(key[0])

                if key[0] not in list_amk:
                    list_amk.append(key[1])

        for letter in sequence:
            if letter not in list_amk:
                self.popup("Invalid sequence character %s" %letter)
                return False
            else:
                continue

        return sequence

    def check_if_digit(self, sequence):
        """Method tests if @sequence is digit

        Attributes:
            sequence (str): entry from gap open/extend penalty entry window
        Return:
            int: integer from @sequence if its digit
        """
        if sequence.isdigit() is False:
            self.popup("Gap penalty isn't a number.")

        else:
            integer = int(sequence)
            return integer


    def read_sequence(self, sequence):
        """Method test what kind of sequence is input from text boxes.

        Attributes:
            sequence(str): sequence from input windows
        Returns:
            str: processed seq
        """
        # check if its FASTA ">" start
        # if its, split and join
        # text widget giving unicode, must to encode
        if isinstance(sequence, unicode):
            sequence = sequence.encode("utf8")

            if sequence.startswith(">"):
                a = sequence.split()
                a.remove(a[0])
                r_sequence = "".join(a)
            else:
                r_sequence = sequence.strip()
        else:
            r_sequence = sequence

        return r_sequence.upper()

    def on_check(self):
        """Method which is called when checking/unchecking checkbutton.

        For NW and SW algorithm it make active options gap open/extend penalty,
        selection of substitution matrix and their labels
        """

        if self.mode.get() == "needleman_wunsch" or self.mode.get() == "smith_waterman":
            self.opt_menu.config(state="normal")
            self.lbl3.config(state="normal")
            self.lbl4.config(state="normal")
            self.lbl5.config(state="normal")
            self.entry_gpopen.config(state="normal")
            self.entry_gpextend.config(state="normal")
        else:
            self.opt_menu.config(state="disabled")
            self.lbl3.config(state="disable")
            self.lbl4.config(state="disable")
            self.lbl5.config(state="disable")
            self.entry_gpopen.config(state="disable")
            self.entry_gpextend.config(state="disable")

    def on_open(self):
        """Method loading sequences from files for text window 1.

        In file options is showed in default FASTA, can load from txt file too.
        """
        ftypes = [('FASTA files', '*.fasta'), ('Text file', '.txt'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes=ftypes)
        fl = dlg.show()

        if fl != '':
            self.area1.delete(1.0, END)
            text = self.read_file(fl)
            self.area1.insert(END, text)

    def on_open2(self):
        """Method loading sequences from files for text window 2.

        In file options is showed in default FASTA, can load from txt file too.
        """
        ftypes = [('FASTA files', '*.fasta'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes=ftypes)
        fl = dlg.show()

        if fl != '':
            self.area2.delete(1.0, END)
            text = self.read_file(fl)
            self.area2.insert(END, text)

    def read_file(self, filename):
        """Reads file selected in on_open and on_open2."""
        # opening file
        f = open(filename, "r")
        text = f.read()
        f.close()
        return text

#root = Tk()
#ex = MainWindow(root)
#root.geometry("800x600")
#root.mainloop()