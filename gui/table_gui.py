from ttk import *
from tkintertable.Tables import TableCanvas
from tkintertable.TableModels import TableModel

class ResultsTable(Frame):
    """Object of result table."""
    def __init__(self, master=None):
        """Initialiazation of window for table

        Attributes:
            master (Frame): object of window in which table appears
        """
        Frame.__init__(self, master)
        master.minsize(width=800, height=800)
        self.pack()
        # creating table model
        self.model = TableModel()
        # creating table from model
        self.table = TableCanvas(self, model=self.model, thefont=("Arial", 10), showkeynamesinheader=False, editable=False)
        # crating end table
        self.table.createTableFrame()
        #self.create_widgets(data)

    def create_widgets(self, data, backtracking_data):
        """Method which imports data and sort them in correct order to table.

        Attributes:
            data(dic): Dictionary with rows and columns of matrix - {row:{column=:value...}}
            backtracking_data (list): Contains coordinates of path in matrix. [(path1), (path2)..]
        """

        # import of data into table model
        self.model.importDict(data)

        # because the data are in dictionary, they are imported unsorted, so rows and columns are in wrong order
        # then in result matrix
        for i in range(self.model.getColumnCount()):
            if i > 1:
                self.model.columnwidths[self.model.getColumnName(self.model.getColumnIndex(str(i)))] = 400
                #self.table.rowheight = 80
            else:
                self.model.columnwidths[self.model.getColumnName(self.model.getColumnIndex(str(i)))] = 50
            self.model.moveColumn(self.model.getColumnIndex(str(i)), i)
        # sorting table with the first column which is supposed to be index column
        self.table.sortTable(0)

        # setting colors to the first two rows and columns as graphical separator from matrix
        for i in range(self.model.getColumnCount()):
            self.table.setcellColor(rows=[0], cols=[i], newColor="blue", key="bg")
            if i > 0:
                self.table.setcellColor(rows=[1], cols=[i], newColor="deep sky blue", key="bg")

        for j in range(self.model.getRowCount()):
            self.table.setcellColor(rows=[j], cols=[0], newColor="blue", key="bg")
            if j > 0:
                self.table.setcellColor(rows=[j], cols=[1], newColor="deep sky blue", key="bg")

        self.table.setRowHeight(70)

        # if backtrackingData are not None = backtracking was checked True,
        # and this will color the backtracked cells

        if backtracking_data is not None:
            # some random selection of colors in case there will be more paths
            colors = ["ivory2", "ivory3", "ivory4", "pink", "purple", "snow3"]
            #print backtracking_data
            for num, path in enumerate(backtracking_data):
                for cell in path:
                    self.table.setcellColor(rows=[cell[0] + 2], cols=[cell[1] + 2], newColor=colors[num], key="bg")


        #self.table.redrawTable()

#root = Tk()
#root.title('Table Test')
#app = ResultsTable(master=root)

#app.mainloop()