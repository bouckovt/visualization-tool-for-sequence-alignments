import numpy as np
import math

def owed(sequence1, sequence2, gap_penalty, match, mismatch):
    m = len(sequence1) + 1
    n = len(sequence2) + 1

    # premade matrix filled with inf values
    matrix = np.full((n, m), np.inf)

    # preset first row/collumn
    for j in range(n):
        matrix[j][0] = j * gap_penalty

    for i in range(m):
        matrix[0][i] = i * gap_penalty

    # ed
    for j in range(1, n):

        for i in range(1, m):

            up = matrix[j - 1][i] + gap_penalty
            left = matrix[j][i - 1] + gap_penalty

            if sequence1[i - 1] == sequence2[j - 1]:
                diagonal = matrix[j - 1][i - 1] + match

            else:
                diagonal = matrix[j - 1][i - 1] + mismatch

            minimum = min(up, left, diagonal)

            matrix[j][i] = minimum
    owed_alignment(sequence1, sequence2, matrix, gap_penalty, match, mismatch)
    return matrix


def owed_alignment(sequence1, sequence2, matrix, gap_penalty, match, mismatch):
    n = matrix.shape[0] - 1
    m = matrix.shape[1] - 1
    paths = []
    final_paths = []
    final_paths = ed_backtracking(n, m, matrix, paths, sequence1, sequence2, final_paths, gap_penalty, match, mismatch)

    for path in final_paths:
        seq1 = ""
        seq2 = ""
        path[1].reverse()
        for couple in path[1]:
            seq1 += couple[0]
            seq2 += couple[1]

        print seq1
        print seq2


def ed_backtracking(n, m, matrix, paths, sequence1, sequence2, final_paths, gap_penalty, match, mismatch):
    coords = []

    if (n >= 0 and m > 0) or (n > 0 and m >= 0):

        if matrix[n][m] == matrix[n - 1][m] + gap_penalty:

            coords.append((n-1, m, "-", sequence2[n-1]))

        if matrix[n][m] == matrix[n][m - 1] + gap_penalty:

            coords.append((n, m - 1, sequence1[m-1], "-"))

        if matrix[n][m] == matrix[n - 1][m - 1] + mismatch :

            coords.append((n - 1, m - 1, sequence1[m-1], sequence2[n-1]))

        if matrix[n][m] == matrix[n - 1][m - 1] + match and sequence1[m - 1] == sequence2[n - 1]:

            coords.append((n - 1, m - 1, sequence1[m-1], sequence2[n-1]))

        if len(paths) == 0:
            new_path = (len(paths)+1, [])
            paths.append(new_path)

        elif len(coords) != 1:
            lal = list(paths[len(paths)-1][1])
            new_path = (len(paths)+1, lal)
            paths.append(new_path)

        for h, coord in enumerate(coords):

            if len(paths) - 1 >= 0:
                paths[len(paths) - 1][1].append((coord[2], coord[3]))

            ed_backtracking(coord[0], coord[1], matrix, paths, sequence1, sequence2, final_paths, gap_penalty, match, mismatch)

    else:
        if len(paths) - 1 >= 0:

            final_paths.append(paths.pop(len(paths)-1))

    return final_paths