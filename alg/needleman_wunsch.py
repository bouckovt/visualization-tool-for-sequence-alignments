from matrices.matrices import *
import numpy as np


def match_pairs(char1, char2, matrix):
    """Function for finding value of substitution for chars in substitution matrix.

    Attributes:
        char1(str): letter from sequence 1
        char2(str): letter from sequence 2

    Returns:
        s(int): value for substitution of char1 with char2
    """
    subst_matrix = eval(matrix)

    char_pair = (char1, char2)
    if char_pair not in subst_matrix:
        char_pair_rev = (char_pair[1], char_pair[0])
        s = subst_matrix[char_pair_rev]

    else:
        s = subst_matrix[char_pair]

    return s


def needleman_wunsch(sequence1, sequence2, gap_open, gap_extend, matrix, backtracking=False):
    """Implementation of Needleman Wunsh algorithm with affine type of gap.

    Note:
        n/j is for axis x in matrix
        m/i is for axis y in matrix
        example matrix[x, y] ~ [j]
        for making sequence 1 horizontal

    Attributes:
        sequence1(str): input sequence 1
        sequence2(str): input sequence 2
        gap_open (int): penalty for opening gap
        gap_extend (int): penalty for extending gap
        matrix (string): name of the matrix, ex. "blosum62", "pam300"
        backtracking (bool): False (default), algorithm runs

    Returns:
        G (numpy.array): Contains global alignment matrix.
        H (numpy.array): Contains horizontal alignment matrix. (values if there would be gap in sequence2)
        V (numpy.array): Contains vertical alignment matrix. (values if there would be gap in sequence1)
        dictionary: Dictionary with rows and columns of matrix - {row:{column=:value...}}

        If backtracking True returns also:
        pairs (list): Contains list of pairs for each backtracked path.
        example: [[("X", "X"), ("X", "X")..],[("X", "X")...]...]
        coords (list): Contains coordinates of path in matrix. [(j, i), (path2)..]
    """
    m = len(sequence1) + 1
    n = len(sequence2) + 1

    # defined pointers
    LEFT, UP, DIAG, fromG = 1, 2, 3, 4

    # creating matrices filled with default values
    G = np.full((n, m), 0)
    H = np.full((n, m), -np.inf)
    V = np.full((n, m), -np.inf)
    # setting 0,0 coordinates in matrices to 0
    pointersG = np.full((n, m), -1)
    pointersG[0,0] = 0  # NONE

    pointersH = np.full((n, m), -1)
    pointersH[0,0] = 0

    pointersV = np.full((n, m), -1)
    pointersV[0,0] = 0

    # preset first row/collumn
    # affine gap
    for j in range(1, n):
        G[j, 0] = -gap_open - (j - 1) * gap_extend
        pointersG[j, 0] = 2

    for i in range(1, m):
        G[0, i] = -gap_open - (i - 1) * gap_extend
        pointersG[0, i] = 1

    # filling matrixes
    for j in range(1, n):

        for i in range(1, m):

            similarity = match_pairs(sequence1[i - 1], sequence2[j - 1], matrix)

            V[j, i] = max(V[j - 1, i] - gap_extend, G[j - 1, i] - gap_open)
            H[j, i] = max(H[j, i - 1] - gap_extend, G[j, i - 1] - gap_open)
            G[j, i] = max(H[j, i], V[j, i], G[j - 1, i - 1] + similarity)

            up = V[j, i]
            left = H[j, i]
            max_score = G[j, i]

            # filling pointer matrix for mat G
            if max_score == up:
                pointersG[j, i] = UP
            elif max_score == left:
                pointersG[j, i] = LEFT
            else:
                pointersG[j, i] = DIAG

            # filling pointer matrix for mat V
            if V[j, i] == V[j - 1, i] - gap_extend:
                pointersV[j, i] = UP
            else:
                pointersV[j, i] = fromG

            # filling pointer matrix for mat H
            if H[j, i] == H[j, i - 1] - gap_extend:
                pointersH[j, i] = LEFT
            else:
                pointersH[j, i] = fromG

    if backtracking:
        coords = nw_backtrack(sequence1, sequence2, pointersG)[0]
        pairs = nw_backtrack(sequence1, sequence2, pointersG)[1]
        return G, H, V, results_to_dict(sequence1, sequence2, G, H, V, pointersG, pointersH, pointersV), pairs, coords

    return G, H, V, results_to_dict(sequence1, sequence2, G, H, V, pointersG, pointersH, pointersV)


def nw_backtrack(sequence1, sequence2, pointersG):
    """Function for backtracking one optimal path in pointer matrix.

    Attributes:
        sequence1 (str): input sequence1
        sequence2 (str): input sequence2
        pointersG (numpy.array): pointers to the cell from where the value came from

    Returns
        [coords](list): List containing list with coordinates of backtracked path
        pairs(list): list of pairs of aligned chars. example [[("X", "-"), ("C", "C")...]]
    """
    j = pointersG.shape[0] - 1
    i = pointersG.shape[1] - 1

    pairs = []
    coords = list()
    coords.append((j, i))
    while pointersG[j, i] != 0:

        if pointersG[j, i] == 1:  # LEFT
            #print j, i
            i -= 1
            coords.append((j, i))
            pairs.append((sequence1[i], "-"))
        elif pointersG[j, i] == 2:  # UP
            #print j, i
            j -= 1
            coords.append((j, i))
            pairs.append(("-", sequence2[j]))
        elif pointersG[j, i] == 3:  # DIAG
            #print j, i
            i -= 1
            j -= 1
            coords.append((j, i))
            pairs.append((sequence1[i], sequence2[j]))
        else:
            print "You got the Problem :/"

    return [coords], pairs


def results_to_dict(sequence1, sequence2, matrix1, matrix2, matrix3, pointers1, pointers2, pointers3):
    """Function creating dictionary from numpy array. Main purpose is make correct input format
    for tkintertable import of data to table.

    Attributes:
        sequence1 (str): input sequence1
        sequence2 (str): input sequence2
        matrix1 (numpy.array): Contains global alignment matrix.
        matrix2 (numpy.array): Contains horizontal alignment matrix. (values if there would be gap in sequence2)
        matrix3 (numpy.array): Contains vertical alignment matrix. (values if there would be gap in sequence1)
        pointers1 (numpy.array): pointers for matrix 1
        pointers2 (numpy.array): pointers for matrix 2
        pointers3 (numpy.array): pointers for matrix 3

    Returns:
        dictionary of rows and columns created from numpy.array, for example
        {"row1" : {"col1" : value11, "col2" : value12, ... "colx" : value1x},
        "row2" : {"col1" : value21, "col2" : value22, ... "colx" : value2x}
        ...}
    """
    G = matrix1
    pointersG = pointers1
    H = matrix2
    pointersH = pointers2
    V = matrix3
    pointersV = pointers3

    multi_mat = {}
    columns = {}
    list_sides = ["END", "LEFT", "UP", "DIAG", "FROM G", ""]
    for j in range(len(sequence2)+3):
        for i in range(len(sequence1)+3):

            if i == 0:
                columns[str(0)] = str(j)
            else:
                if j == 0:
                    columns[str(i)] = str(i)
                elif j == 1:
                    if i == 1:
                        columns[str(i)] = "*"
                    elif i == 2:
                        columns[str(i)] = "-"
                    else:
                        columns[str(i)] = sequence1[i - 3]
                else:
                    if j == 2:
                        if i == 1:
                            columns[str(i)] = "-"
                        else:
                            s1 = "G(%s) | %s\n" % (G[j - 2, i - 2], list_sides[int(pointersG[j - 2, i - 2])])
                            s2 = "H(%s) | %s\n" % (H[j - 2, i - 2], list_sides[int(pointersH[j - 2, i - 2])])
                            s3 = "V(%s) | %s" % (V[j - 2, i - 2], list_sides[int(pointersV[j - 2, i - 2])])
                            s = s1 + s2 + s3
                            columns[str(i)] = s
                    else:
                        if i == 1:
                            columns[str(i)] = sequence2[j - 3]
                        else:
                            s1 = "G(%s) | %s\n" % (G[j - 2, i - 2], list_sides[int(pointersG[j - 2, i - 2])])
                            s2 = "H(%s) | %s\n" % (H[j - 2, i - 2], list_sides[int(pointersH[j - 2, i - 2])])
                            s3 = "V(%s) | %s" % (V[j - 2, i - 2], list_sides[int(pointersV[j - 2, i - 2])])
                            s = s1 + s2 + s3
                            columns[str(i)] = s

        multi_mat[str(j)] = columns
        columns = {}

    return multi_mat

