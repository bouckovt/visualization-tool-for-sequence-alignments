import numpy as np


def edit_distance(sequence1, sequence2, backtracking=False):
    """Implementation of edit distance algorithm.

    Note:
        n/j is for axis x in matrix
        m/i is for axis y in matrix
        example matrix[x, y] ~ [j]
        for making sequence 1 horizontal

    Attributes:
        sequence1(str): input sequence 1
        sequence2(str): input sequence 2
        backtracking (bool): False (default), algorithm runs

    Returns:
        matrix (numpy.array): Contains global alignment matrix.
        dictionary: Dictionary with rows and columns of matrix - {row:{column=:value...}}
        if backtracking true it returns also final_paths:
        list of paths, with information about the coordinates and character alignment, for example:
        [[(coordX, coordY, char_seq1, char_seq2), (coordX, coordY, char_seq1, char_seq2)...],
        ..., [(coordX, coordY, char_seq1, char_seq2)]]
    """
    m = len(sequence1) + 1
    n = len(sequence2) + 1

    LEFT, UP, DIAG = 1, 2, 3
    # premade matrix filled with inf values
    matrix = np.full((n, m), np.inf)
    pointers = np.full((n, m), -1)
    pointers[0,0] = 0 # NONE

    matrix[0,0] = 0

    # preset first row/collumn
    for j in range(1, n):
        matrix[j, 0] = j
        pointers[j, 0] = 2
    for i in range(1, m):
        matrix[0, i] = i
        pointers[0, i] = 1
    # ed
    for j in range(1, n):

        for i in range(1, m):

            up = matrix[j - 1, i] + 1
            left = matrix[j, i - 1] + 1

            if sequence1[i - 1] == sequence2[j - 1]:
                diagonal = matrix[j - 1, i - 1] + 0

            else:
                diagonal = matrix[j - 1, i - 1] + 1

            minimum = min(up, left, diagonal)

            if minimum == up:
                pointers[j, i] = UP
            elif minimum == left:
                pointers[j, i] = LEFT
            else:
                pointers[j, i] = DIAG

            matrix[j, i] = minimum
    if backtracking:
        final_paths = ed_alignment(sequence1, sequence2, matrix)
        return final_paths, results_to_dict(sequence1, sequence2, matrix, pointers), matrix

    return matrix, results_to_dict(sequence1, sequence2, matrix, pointers)


def ed_alignment(sequence1, sequence2, matrix):
    """Function which starts recursive function for backtracking
    sequence1 (str): input sequence1
    sequence2 (str): input sequence2
    matrix1 (numpy.array): Contains global alignment matrix.

    Returns
        final_paths: list of paths, with information about the coordinates and character alignment, for example:
                [[(coordX, coordY, char_seq1, char_seq2), (coordX, coordY, char_seq1, char_seq2)...],
                ..., [(coordX, coordY, char_seq1, char_seq2)]]
    """
    n = matrix.shape[0] - 1
    m = matrix.shape[1] - 1
    paths = []
    final_paths = []
    final_paths = ed_backtracking(n, m, matrix, paths, sequence1, sequence2, final_paths)


    return final_paths


def ed_backtracking(n, m, matrix, paths, sequence1, sequence2, final_paths):
    """Function for recursive backtracking of all optimal path from matrix.

    Attributes:
        n (int): x index of the last row
        m (int): y index of the last column
        matrix (numpy. array): matrix with values from edit distance algoritm
        paths (list): helper list for saving sub results
        sequence1 (str): input sequence1
        sequence2 (str): input sequence2
        final_paths: list of the finnished paths

    Returns
        final_paths: list of paths, with information about the coordinates and character alignment, for example:
            [[(coordX, coordY, char_seq1, char_seq2), (coordX, coordY, char_seq1, char_seq2)...],
            ..., [(coordX, coordY, char_seq1, char_seq2)]]
    """
    coords = []

    if (n >= 0 and m > 0) or (n > 0 and m >= 0):

        if matrix[n, m] == matrix[n - 1, m] + 1:

            coords.append((n - 1, m, "-", sequence2[n - 1]))

        if matrix[n, m] == matrix[n, m - 1] + 1:

            coords.append((n, m - 1, sequence1[m - 1], "-"))

        if matrix[n, m] == matrix[n - 1, m - 1] + 1 :

            coords.append((n - 1, m - 1, sequence1[m-1], sequence2[n-1]))

        if matrix[n, m] == matrix[n - 1, m - 1] + 0 and sequence1[m - 1] == sequence2[n - 1]:

            coords.append((n - 1, m - 1, sequence1[m-1], sequence2[n-1]))

        if len(paths) == 0:
            new_path = [(n, m, "", "")]
            paths.append(new_path)

        elif len(coords) != 1:
            lal = list(paths[len(paths)-1])
            new_path = lal
            paths.append(new_path)

        for h, coord in enumerate(coords):

            if len(paths) - 1 >= 0:
                paths[len(paths) - 1].append(coord)

            ed_backtracking(coord[0], coord[1], matrix, paths, sequence1, sequence2, final_paths)

    else:
        if len(paths) - 1 >= 0:

            final_paths.append(paths.pop(len(paths)-1))

    return final_paths

def results_to_dict(sequence1, sequence2, matrix1, pointers):
    """Function creating dictionary from numpy array. Main purpose is make correct input format
    for tkintertable import of data to table.

    Attributes:
        sequence1 (str): input sequence1
        sequence2 (str): input sequence2
        matrix1 (numpy.array): Contains global alignment matrix.
        pointers1 (numpy.array): pointers for matrix 1,
                                shows pointers from cells to cells from where their value came from

    Returns:
        dictionary of rows and columns created from numpy.array, for example

        {"row1" : {"col1" : value11, "col2" : value12, ... "colx" : value1x},
            "row2" : {"col1" : value21, "col2" : value22, ... "colx" : value2x}
    """
    G = matrix1
    list_sides = ["END", "LEFT", "UP", "DIAG", ""]
    multi_mat = {}
    columns = {}
    for j in range(len(sequence2)+3):
        for i in range(len(sequence1)+3):
            #print columns.keys()
            if i == 0:
                columns[str(0)] = str(j)
            else:
                if j == 0:

                    columns[str(i)] = str(i)
                elif j == 1:

                    if i == 1:
                        columns[str(i)] = "*"
                    elif i == 2:
                        columns[str(i)] = "-"
                    else:
                        columns[str(i)] = sequence1[i - 3]
                else:
                    if j == 2:
                        if i == 1:
                            columns[str(i)] = "-"

                        else:
                            s = "G(%s) | %s" % (G[j - 2, i - 2], list_sides[int(pointers[j - 2, i - 2])])
                            columns[str(i)] = s

                            #print sequence1[i]
                    else:
                        if i == 1:

                            columns[str(i)] = sequence2[j - 3]
                        else:
                            s = "G(%s) | %s" % (G[j - 2, i - 2], list_sides[int(pointers[j - 2, i - 2])])
                            columns[str(i)] = s
                            #print sequence1[i]

        multi_mat[str(j)] = columns
        columns = {}

    return multi_mat