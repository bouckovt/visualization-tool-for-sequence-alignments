
def hamming_distance(sequence1, sequence2):
    """Function for counting hamming distance.

    Attributes:
        sequence1(str): input sequence 1
        sequence2(str): input sequence 2
        backtracking (bool): False (default), algorithm runs

    """
    hd = 0
    matches_indexes = []

    for i in range(len(sequence1)):
        if sequence1[i] != sequence2[i]:
            hd += 1

        else:
            matches_indexes.append(i)


    return hd, matches_indexes